#include <stdio.h>
#include "misc.h"

void fail(void)
{
    puts("wrong usage: Run cinit -h for help.");
}

void help(void)
{
    puts("usage:");
    puts("cinit       initialize C-project");
    puts("cinit -h    print this help");
    puts("cinit -i    print program info");
}

void info(void)
{
    puts("cinit (Cinit) 1.0");
    puts("Copyright (c) 2022 Benjamin Mross");
    puts("This program is distributed under the MIT license.");
}
