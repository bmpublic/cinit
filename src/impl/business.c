#include <stdio.h>
#include <sys/stat.h>
#include "business.h"

void dinit(void)
{
    mode_t mode;
    mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;
    mkdir("src", mode);
    mkdir("src/impl", mode);
    mkdir("src/incl", mode);
    mkdir("src/libs", mode);
}

void finit(void)
{
    FILE *file;
    file = fopen("src/impl/main.c", "w");
    fprintf(file, "#include \"config.h\"\n\nint main(int argc, char *argv[])\n{\n    return 0;\n}");
    fclose(file);
    file = fopen("src/incl/config.h", "w");
    fclose(file);
    file = fopen("makefile", "w");
    fprintf(file, "NAME := prog\n");
    fprintf(file, "PREFIX := /usr/local\n");
    fprintf(file, "CC := gcc\n\n");
    fprintf(file, "override SRC := $(wildcard src/impl/*.c)\n");
    fprintf(file, "override OBJ := $(SRC:src/impl/%%.c=obj/%%.o)\n");
    fprintf(file, "override LBS := $(wildcard src/libs/*.a)\n");
    fprintf(file, "override LBA := $(LBS:src/libs/lib%%.a=-l %%)\n");
    fprintf(file, "override BIN := bin/$(NAME)\n");
    fprintf(file, "override DIR := $(DESTDIR)$(PREFIX)/bin\n\n");
    fprintf(file, ".PHONY: all test install clean uninstall\n\n");
    fprintf(file, "all: $(BIN)\n\n");
    fprintf(file, "$(BIN): $(OBJ) | bin\n");
    fprintf(file, "\t@printf \"LD\\t$@\\n\"\n");
    fprintf(file, "\t@$(CC) $(LDFLAGS) -L src/libs $(LBA) -o $@ $(OBJ)\n");
    fprintf(file, "\t@strip $@\n\n");
    fprintf(file, "obj/%%.o: src/impl/%%.c | obj\n");
    fprintf(file, "\t@printf \"CC\\t$@\\n\"\n");
    fprintf(file, "\t@$(CC) $(CFLAGS) $(CPPFLAGS) -I src/incl -c -o $@ $<\n\n");
    fprintf(file, "obj bin:\n");
    fprintf(file, "\t@printf \"MD\\t$@\\n\"\n");
    fprintf(file, "\t@mkdir -p $@\n\n");
    fprintf(file, "test:\n");
    fprintf(file, "\t@printf \"EX\\t$(BIN)\\n\"\n");
    fprintf(file, "\t@$(BIN)\n\n");
    fprintf(file, "install:\n");
    fprintf(file, "\t@printf \"IN\\t$(DIR)/$(NAME)\\n\"\n");
    fprintf(file, "\t@install -d $(DIR)\n");
    fprintf(file, "\t@install -m 755 $(BIN) $(DIR)/$(NAME)\n\n");
    fprintf(file, "clean:\n");
    fprintf(file, "\t@printf \"RM\\tobj bin\\n\"\n");
    fprintf(file, "\t@rm -rf obj bin\n\n");
    fprintf(file, "uninstall:\n");
    fprintf(file, "\t@printf \"RM\\t$(DIR)/$(NAME)\\n\"\n");
    fprintf(file, "\t@rm -f $(DIR)/$(NAME)\n");
    fclose(file);
}
