NAME := cinit
PREFIX := /usr/local
CC := gcc

override SRC := $(wildcard src/impl/*.c)
override OBJ := $(SRC:src/impl/%.c=obj/%.o)
override LBS := $(wildcard src/libs/*.a)
override LBA := $(LBS:src/libs/lib%.a=-l %)
override BIN := bin/$(NAME)
override DIR := $(DESTDIR)$(PREFIX)/bin

.PHONY: all test install clean uninstall

all: $(BIN)

$(BIN): $(OBJ) | bin
	@printf "LD\t$@\n"
	@$(CC) $(LDFLAGS) -L src/libs $(LBA) -o $@ $(OBJ)
	@strip $@

obj/%.o: src/impl/%.c | obj
	@printf "CC\t$@\n"
	@$(CC) $(CFLAGS) $(CPPFLAGS) -I src/incl -c -o $@ $<

obj bin:
	@printf "MD\t$@\n"
	@mkdir -p $@

test:
	@printf "EX\t$(BIN)\n"
	@$(BIN)

install:
	@printf "IN\t$(DIR)/$(NAME)\n"
	@install -d $(DIR)
	@install -m 755 $(BIN) $(DIR)/$(NAME)

clean:
	@printf "RM\tobj bin\n"
	@rm -rf obj bin

uninstall:
	@printf "RM\t$(DIR)/$(NAME)\n"
	@rm -f $(DIR)/$(NAME)
