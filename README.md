# Cinit
Initializes a new C-project.

# Dependencies
**binutils** (to strip symbols from binary)

**gcc** (to compile the sources)

**make** (to use the makefile)

# Build
To build and install the program you can use the makefile as follows:

    make
    make install

The makefile also includes some optional targets:

    make test
    make clean
    make uninstall

# Usage
Run the new command:

    cinit
    cinit -h
    cinit -i
